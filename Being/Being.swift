//
//  Being.swift
//  Being
//
//  Created by Bogdan Stasiuk on 7/16/18.
//  Copyright © 2018 Skylum. All rights reserved.
//

enum Sex: Int8 {
    case Female = 0
    case Male = 1
    
    static func rand() -> Sex {
        return Sex(rawValue:(Math.rand(inRange: (self.Female.rawValue...self.Male.rawValue))))!
    }
}

protocol Being: Living, Growing, Saying, Reproducing {
    var sex: Sex { get set }
    var age: UInt32 { get set }
    var name: String { get set }
    
    var alive: Bool { get set }
    var thresholdAge: UInt32 { get set }
    
    init()
    init(naming: String)
    
    static func instantiate() -> Self
}

extension Being {
    //MARK: Public methods
    
    // MARK: -Init
    
    init(naming: String) {
        self.init()
        sex = .rand()
        age = UInt32(Math.rand(inRange: 1...99))
        name = naming
        alive = true
        thresholdAge = 100
    }
    
    static func instantiate() -> Self {
        return self.init(naming: "")
    }
    
    // MARK: -Others
    
    func description() -> String {
        var description: String
        if isAlive() {
            description = "My name is " + sayName() + ". I'm " + sayAge() + " years old " + saySex()
            if let father = father {
                description += "\nMy father is " + father.sayName()
            }
            if let mother = mother {
                description += "\nMy mother is " + mother.sayName()
            }
        } else {
            description = "I'm dead!"
        }
        return description
    }
}

extension Living where Self: Being {
    func isAlive() -> Bool {
        return alive
    }
}

extension Growing where Self: Being {
    mutating func grow() -> Bool {
        if !isAlive() {
            return false
        }
        
        age+=1
        if age >= thresholdAge {
            alive = false
        }
        return true
    }
}

extension Saying where Self: Being {
    func sayName() -> String {
        return name
    }
    
    func sayAge() -> String {
        return String(age)
    }
    
    func saySex() -> String {
        return String(sex == .Female ? "woman" : "man")
    }
}

extension Reproducing where Self: Being {
    static func makeChild(mother: inout Self, father: inout Self) -> Self? {
        guard mother.sex != father.sex, mother.age >= 18, father.age >= 18 else {
            return nil
        }
        
        var child = Self.instantiate()
        child.mother = mother
        child.father = father
        child.age = 0
        
        if let children = mother.children {
            mother.children = children + [child]
        }  else {
            mother.children = [child]
        }
        
        if let children = father.children {
            father.children = children + [child]
        }  else {
            father.children = [child]
        }
        
        return child
    }
}
