//
//  BeingStruct.swift
//  Being
//
//  Created by Bogdan Stasiuk on 7/4/18.
//  Copyright © 2018 Skylum. All rights reserved.
//

struct BeingStruct: Being {
    var sex: Sex = .Male
    var age: UInt32 = 0
    var name: String = ""
    
    var alive: Bool = true
    var thresholdAge: UInt32 = 0

    private var _mother: [BeingStruct]?
    var mother: BeingStruct? {
        set {
            _mother = newValue.map{ [$0] }
        }
        get {
            return _mother?.first
        }
    }
    
    private var _father: [BeingStruct]?
    var father: BeingStruct? {
        set {
            _father = newValue.map{ [$0] }
        }
        get {
            return _father?.first
        }
    }
    
    var children: [BeingStruct]?
}
