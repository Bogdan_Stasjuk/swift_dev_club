//
//  BeingClass.swift
//  Being
//
//  Created by Bogdan Stasiuk on 7/5/18.
//  Copyright © 2018 Skylum. All rights reserved.
//

final class BeingClass: Being {
    var sex: Sex = .Male
    var age: UInt32 = 0
    var name: String = ""
    
    var alive: Bool = true
    var thresholdAge: UInt32 = 0

    weak var mother: BeingClass?
    weak var father: BeingClass?
    var children: [BeingClass]?
    
    deinit {
        Debug.print(self)
    }
}
