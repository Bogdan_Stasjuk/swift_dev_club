//
//  Growing.swift
//  Being
//
//  Created by Bogdan Stasiuk on 7/8/18.
//  Copyright © 2018 Skylum. All rights reserved.
//

protocol Growing {
    mutating func grow() -> Bool
}
