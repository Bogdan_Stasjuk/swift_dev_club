//
//  Saying.swift
//  Being
//
//  Created by Bogdan Stasiuk on 7/8/18.
//  Copyright © 2018 Skylum. All rights reserved.
//

protocol Saying {
    func sayName() -> String
    func saySex() -> String
    func sayAge() -> String
}
