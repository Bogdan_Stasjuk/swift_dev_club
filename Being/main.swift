//
//  main.swift
//  Being
//
//  Created by Bogdan Stasiuk on 7/4/18.
//  Copyright © 2018 Skylum. All rights reserved.
//

do {
    //MARK: struct

    var beingStruct = BeingStruct(naming: "Vasya")
    print(beingStruct)
    Debug.print(beingStruct.description())

    if beingStruct.grow() {
        print("1 year later")
        print("name:", beingStruct.sayName())
        print("age:", beingStruct.sayAge())
        print("sex:", beingStruct.saySex())
    }

    var timePeriod = 0
    while beingStruct.grow() {
        timePeriod+=1
    }
    print("\(timePeriod) years later")
    print(beingStruct.description())
    print()

    //MARK: class

    var beingClass = BeingClass(naming: "Kolya")
    print(beingClass)
    print(beingClass.description())

    if beingClass.grow() {
        print("1 year later")
        print("name:", beingClass.sayName())
        print("age:", beingClass.sayAge())
        print("sex:", beingClass.saySex())
    }

    timePeriod = 0
    while beingClass.grow() {
        timePeriod+=1
    }
    print("\(timePeriod) years later")
    print(beingClass.description())
    print()

    // MARK: Reproducing Struct
    print("Reproducing Struct")

    var father = BeingStruct(naming: "")
    father.age = 18
    father.sex = .Male
    father.name = "Kolya"

    var mother = BeingStruct(naming: "")
    mother.age = 18
    mother.sex = .Female
    mother.name = "Olya"

    let child = BeingStruct.makeChild(mother: &mother, father: &father)
    if var child = child {
        child.name = child.sex == .Male ? "Dima" : "Katya"
        print(child.description() as Any)
        print()
    }

    // MARK: Reproducing Class
    print("Reproducing Class")

    var father2 = BeingClass(naming: "")
    father2.age = 80
    father2.sex = .Male
    father2.name = "Petya"
    
    var mother2 = BeingClass(naming: "")
    mother2.age = 20
    mother2.sex = .Female
    mother2.name = "Mariya"
    
    let child2 = BeingClass.makeChild(mother: &mother2, father: &father2)
    if let child = child2 {
        child.name = child.sex == .Male ? "Svyatoslav" : "Nadezhda"
        print(child.description() as Any)
        print()
    }
}
