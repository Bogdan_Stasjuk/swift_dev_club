//
//  Math.swift
//  Being
//
//  Created by Bogdan Stasiuk on 7/6/18.
//  Copyright © 2018 Skylum. All rights reserved.
//

import Darwin

class Math {
    public static func rand<T: SignedInteger>(inRange range: ClosedRange<T>) -> T {
        return T(Int64(arc4random()) % Int64((range.upperBound - range.lowerBound + 1) + range.lowerBound))
    }
}
