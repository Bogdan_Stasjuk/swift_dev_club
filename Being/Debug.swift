//
//  Debug.swift
//  Being
//
//  Created by Bogdan Stasiuk on 8/1/18.
//  Copyright © 2018 Skylum. All rights reserved.
//

import Cocoa

class Debug {
    static func print(_ message: Any, file: String = #file, function: String = #function, line: Int = #line ) {
        Swift.print("\((file as NSString).lastPathComponent):\(line) \(function): \(message)")
    }
}
