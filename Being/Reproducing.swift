//
//  Reproducing.swift
//  Being
//
//  Created by Bogdan Stasiuk on 7/26/18.
//  Copyright © 2018 Skylum. All rights reserved.
//

protocol Reproducing {
    var mother: Self? { get set }
    var father: Self? { get set }
    var children: [Self]? { get set }
    
    static func makeChild(mother: inout Self, father: inout Self) -> Self?
}
