//
//  main.swift
//  NumAsBinaryString
//
//  Created by Bogdan Stasiuk on 7/4/18.
//  Copyright © 2018 Skylum. All rights reserved.
//

import Foundation

// MARK: direct solution

print("direct solution")
func bits(of byte: UInt8) -> String {
    return (0 ..< BYTE_SIZE).reversed().reduce("") { $0 + String(byte >> $1 & 1) }
}
print(bits(of: 5))

// MARK: generic

print("\ngeneric")
func bits<T: FixedWidthInteger>(of integer: T) -> String {
    return (0 ..< Int32(integer.bitWidth) / BYTE_SIZE).reversed().reduce("") { $0 + ($0.isEmpty ? "" : " ") + String(bits(of: UInt8.init(bitPattern: Int8(integer >> ($1 * BYTE_SIZE))))) }
}
print(bits(of: UInt8(5)))
print(bits(of: 5))
print(bits(of: -5))

// MARK: protocol extension

print("\nprotocol extension")
extension FixedWidthInteger {
    public var binaryString: String {
        return bits(of: self)
    }
}
print(UInt8(5).binaryString)
print((5).binaryString)
print((-5).binaryString)

